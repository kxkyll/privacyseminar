%-------------------------------------------------%
\section{Introduction}
%-------------------------------------------------%
\large
Cloud computing services are a tempting alternative when enterprises are selecting technologies for their IT strategies. Instead of purchasing and maintaining own hardware or software enterprises can choose to lease resources and select software products from cloud providers. When making the decision of the cloud usage the price is often the dominant factor~\cite{Mar:2011}. One significant advantage of cloud usage is the flexibility of the used services. This makes it possible to scale capacity up or down based on the current need, which may result in reduced costs. 

Cloud services are offered in a variety of service levels. The most common service levels are presented in Figure~\ref{fig:k1}: Software as a Service, SaaS, Platform as a Service, PaaS, and Infrastructure as a Service, IaaS. Saas offering includes ready to use software like Microsoft Office 365. PaaS offering comprises of ready to use platforms for developing and deploying software including databases, web servers and development environments, like Heroku. IaaS, offering contains servers, virtual machines and storage with backup facilities, like Amazon AWS.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.40]{CloudServiceLevels.png}
\caption
{Cloud Service Level Offering }
\label{fig:k1}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
%\vspace*{1px}

When enterprises are considering cloud services the greatest concern relates to privacy and security aspects~\cite{Zha:2010}. The data protection legislation is enforcing companies to take privacy aspects seriously. Enterprises are becoming more security-aware and privacy-aware also due to the publicly reported privacy and security incidents. The materialized information disclosure can pose severe harm to businesses. 

Cloud computing involves enterprises to the infrastructure that is shared between organisations and it is off-premise \cite{Pea:2009}. It is no longer possible for one organisation to have access everywhere in a cloud in order to gather, store and analyze threats. The flexibility of cloud offerings, the thing that makes cloud usage very tempting for enterprises, causes rapid changes in the cloud environment. These changes make it difficult for enterprises to maintain consistent security standards \cite{McG:2014} and to assess security and privacy levels of cloud providers. 
Even if enterprises would use the best available security methods to protect their data there is always the possibility that information falls into the wrong hands. Therefore, it is good to use proactive means like anonymization or pseudonymization to mitigate the consequences of possible data leakage.  

Information privacy means controlling one's own information. The subject of the data should have control of what is published and to whom. Traditionally private information has been secured with a national legislation. European Parliament has harmonized the data protection legislation across the European Union \cite{EU:2015}. This will improve the situation within the European Union, but these actions are not yet adequate as many cloud providers operate across continents. Due to differences in legislation, it is challenging for companies both as cloud users and cloud providers to be compliant with privacy legislation.   

This work presents how anonymization and pseudonymization can be used to preserve privacy in a public cloud environment. Anonymization is a process where the data is manipulated in a way that the identifying details of a subject are removed \cite{ITU:2013,ISO:2011}. In pseudonymization, the direct identifiers like names are replaced with pseudonyms \cite{ITU:2013,ISO:2011}. 

The work is organised as follows, section 2 covers anonymization method, section 3 explains pseudonymization method, section 4 introduces pseudonymization proof of concept and section 5 concludes.


%-------------------------------------------------%
%\newpage
%\vspace*{2px}
\section{Anonymization}
%-------------------------------------------------%
%http://ec.europa.eu/justice/data-protection/article-29/documentation/opinion-recommendation/files/2014/wp216_en.pdf
Anonymization is a privacy enhancement technique used to either remove or modify identifying parts of data to protect the privacy of a subject. Anonymization is sometimes used as an umbrella term to mean all operations, including pseudonymization, to hide the identity of a subject. In this work, we restrict anonymization to mean operations that aim to irreversibly prevent identification of the subject \cite{ISO:2011,EU:2014}. Pseudonymization allows re-identification and is discussed in section 3.

When data is used for statistical or research purposes it is not always necessary to connect the data to the real identity of a subject. This type of data analysis is often performed with large data sets in a cloud environment. Before data is transferred to a cloud storage it needs to be anonymized to prevent insulting privacy \cite{Str:2013}. 

Data collections typically contain primary identifiers that connect data to the subject. Attributes like name, social security number or enterprise identification number are primary identifiers. Data collections contain also attributes that are quasi-identifiers. Quasi-identifiers are attributes, like an address that can be used to identify the subject when combined with some other information. In anonymization process, these attributes need to be first identified and then either removed or changed in a way that they no longer reveal the identity of the subject.
\newpage
Those attributes that can not be removed without making the data useless are either obscured, generalized or suppressed. When these modifications are planned it is good to know what is the goal of the operations performed after anonymization. This will help to keep data usable after anonymization. A common method to obscure an attribute is to replace all possible values with a constant. For instance, all names can be replaced with an asterisk. 

There are some other less secure methods for obscuring, like shifting, that adds a constant value to an attribute or enumeration that adds such a value to the attribute that preserves the order of the original attributes \cite{Sed:2012}. With suppressing the attribute is partly hidden. Generalizing an attribute means changing the accuracy of the value to be more general. Birthday can be generalized by changing the data to contain only the year of birth. The concept of anymization is presented in Figure~\ref{fig:k0}: The data collection contains fields phone number and name that need to be either removed or modified so that they do not point to a single data subject. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.40]{Anonymous.png}
\caption
{The Concept of Anonymization}
\label{fig:k0}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Data should not be considered anonymous only after removing obvious identifiers. There are various examples of re-identifying individuals by combining two or more anonymous looking data sets. This kind of linking effect can be hard to control as it would require knowing not only own dataset but also other publicly available datasets. The classical example is the study that showed that 87 \% of people in the United states were identified by the combination of zip code, birthday, and gender \cite{Swe:1997}. Another widely publicized example is the Netflix movie ranking database that was partly de-anonymized by combining the data to public Internet Movie Database \cite{Nar:2008}. 
\newpage
The k-anonymity and l-diversity models are designed to overcome this type of linking effects. Both of these models modify data in a way that in the end the data contains a certain amount of similar records that conceals the identity of a single subject. It is important to understand how these models work and what are their weaknesses. This makes it possible to assess how good anonymity level can be achieved for a particular dataset.

The k-anonymity model \cite{Swe:2002} modifies data in such way that each record in a dataset is indistinguishable from a number of other records. This is achieved first by removing or obscuring those attributes that directly identify the subject like name or social security number and second by generalizing or suppressing such attributes that can identify subject when linked to external information. The k-anonymity model contains the assumption that the data owner can identify all fields in the data that can be used to link data with external information \cite{Swe:2002}. 
Table~\ref{table:t1} contains data of people with age, zip code and a diagnosed disease. This represents the data before anonymization is done. 

\begin{table}[ht]
\parbox{.50\linewidth}
%\parbox{1.0\linewidth}
{
\centering
\caption{Data, no anonymization}
\begin{tabular}{lll}
\hline
Age & Zip code & Disease  \\
\hline
21    & 13045 & Heart disease  \\
25    & 13041 & Heart disease  \\
28    & 13028 & Heart disease  \\
33    & 14033 & Cancer      \\
35    & 14025 & Viral infection   \\
38    & 14044 & Heart disease     \\
42    & 11031 & Cancer      \\
44    & 11045 & Viral infection   \\
48    & 11054 & Cancer     \\

\hline
\end{tabular}
\label{table:t1}
}
%\vspace*{5px}\newline
\hfill
\parbox{.50\linewidth}{
%\parbox{1.0\linewidth}{
\centering
\caption{k-anonymous data}
\begin{tabular}{lll}
\hline
Age & Zip code & Disease  \\
\hline
2*    & 130* & Heart disease  \\
2*    & 130* & Heart disease  \\
2*    & 130* & Heart disease  \\
\hline
3*    & 140* & Cancer      \\
3*    & 140* & Viral infection   \\
3*    & 140* & Heart disease     \\
\hline
4*    & 110* & Cancer      \\
4*    & 110* & Viral infection   \\
4*    & 110* & Cancer     \\
\hline
\end{tabular}
\label{table:t2}
}
\end{table}

Table~\ref{table:t2} shows the data after it has been k-anonymized. In this simplified example, the k is 3. This is visible on the table so that each block contains 3 records where the identifying fields contain same values. Blocks are separated by horizontal lines. 
From this anonymized data can be computed for example the number of diseases in all age groups by the living district.
								
K-anonymization is vulnerable to homogeneity attack \cite{Mac:2006}. In homogeneity attack, the quasi-identifiers are generalized in a way that in some block all the sensitive information is the same like in table~\ref{table:t2} in the age group 2*. If an attacker knows that someone in age group 2* is in this dataset, then the attacker gets to know that the diagnosis for this person is heart disease \cite{Mac:2006}.
\newpage
K-anonymization is also vulnerable to background knowledge attack \cite{Mac:2006}. In background knowledge attack an attacker has some information that helps to cut back the options in a way that sensitive information is revealed with high certainty \cite{Mac:2006}. If an attacker has for example background information that a person in age group 4* does not have cancer, then attacker gets to know that the diagnosis is a viral infection in the table ~\ref{table:t2} case. 

L-diversity is a technique developed to overcome the weaknesses of k-anonymity. In l-diversity, the quasi-identifiers are suppressed and generalized but in addition, each block must contain at least l well-represented values for the sensitive attribute \cite{Mac:2006}. Table~\ref{table:t3} contains data that needs to be anonymized. The name is direct identifier and age and zip code are quasi-identifiers and diagnosis is sensitive data.    

\begin{table}[ht]
\parbox{.50\linewidth}{
%\parbox{1.0\linewidth}{
\centering
\caption{Data, no diversification}
\begin{tabular}{llll}
\hline
Name    & Age & Zip code & Diagnosis  \\
\hline
Kathy     & 36    & 00690 & Cancer      \\
John      & 32    & 00660 & Heart disease  \\
Mary     & 33    & 00690  & Viral infection    \\
Jack      & 41    & 00520 & Viral infection      \\
Leo     & 45    & 00550   & Cancer   \\
Ann      & 44    & 00560  & Heart disease     \\
\hline
\end{tabular}
\label{table:t3}
}\hfill
\parbox{.50\linewidth}{
%\parbox{1.0\linewidth}{
\centering
\caption{3-diverse data}
\begin{tabular}{llll}
\hline
Name    & Age & Zip code & Diagnosis  \\
\hline
*     & $\leq$ 40    & 006* & Cancer      \\
*      & $\leq$ 40   & 006* & Heart disease      \\
*     & $\leq$ 40    & 006*  & Viral infection    \\
\hline
*      & $>$ 40    & 005* & Viral infection      \\
*     & $>$ 40    & 005*   & Cancer   \\
*     & $>$ 40   & 005*  & Heart disease     \\
\hline
\end{tabular}
\label{table:t4}
}
\end{table}
%\newpage
%\vspace*{1px}

%\vspace*{5px}\newline
Table~\ref{table:t4} contains data after it has been l-diversified. The field name has been modified to contain asterisk in all cases, the field age has been generalized to categorical variables, the field zip code has been suppressed and each of the two blocks contains 3 different values of each diagnosis. This makes data 3-diverse.\newline

L-diversity method has been critisized that it may be difficult to achieve in situations where the sensitive attribute can have for example only two values and either of these values is highly dominant \cite{Li:2007}. This can be mitigated by inserting false data, fictious subjects to the data to achieve l-diversity. These fictious records must be of course ignored when some computations are performed.


% weaknesses of l-diversity
% t-closeness?
%What was learned: data must be well studied to identify fields that need modification
%connection to the aimed goals:
%remaining aspect: when the identity is needed  
Anonymization is an adequate technique to preserve privacy that retains the usability of the data for an analysis. The anonymization should be done before data is stored in a cloud environment. The data must be well studied and understood to avoid weaknesses of anonymization methods. To protect privacy knowing own data is not enough, it is also necessary to follow what related data external information sources are publishing. It is also good to remember that anonymization does not give guarantees, but needs continuous monitoring. Once a dataset is truly anonymised and individuals are no longer identifiable, European data
protection law no longer applies \cite{EU:2014} and data can be stored and analyzed in a cloud without a fear of legal consequences. 
Sometimes it is necessary to retain possibility to connect the data to the real identity. In these cases the appropriate privacy enhancement technique is pseudonymization.  
 
 
\newpage
%\vspace*{2px}
%-------------------------------------------------%
\section{Pseudonymization}
%-------------------------------------------------%	 
Pseudonymization is a privacy enhancement technique that protects privacy by replacing the identifier of a subject with a pseudonym. Pseudonymization can be used in situations where private data is collected from same subject repeatedly and it is necessary to connect these pieces of data to the same subject. This is typical for long-term data collection. Pseudonymization also enables that the data can be later on connected to the real identity of a subject. This is called re-identification. The concept of pseudonymization is presented in Figure~\ref{fig:k3}: The identifying field of a person is replaced with a false name, pseudonym, that hides the identity of a subject of collected health data. \newline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.40]{pseudonym.png}
\caption
{The Concept of Pseudonymization}
\label{fig:k3}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
%\vspace*{1px}

Pseudonymization can be done as a one-way operation or as a two-way operation. In one-way operation, the subject is given a pseudonym without the need to retrieve the true identity of a subject. In two-way operation the original value is mapped to the pseudonym, allowing the re-identification later on. This requires that mapping of pseudonyms to their original values are kept in a secure place. In order to protect privacy the pseudonymization needs to be done before the data is transferred to a cloud \cite{Str:2013}. The possible re-identification needs to be done as well after data is transferred from cloud to a secure enclave.

A pseudonym can be any code that hides the real identity of a data subject, like a random number or a code name. Hashing is often used to produce pseudonyms. Hash functions have the benefit that they work deterministically, producing always the same pseudonym for the same subject \cite{Abd:2014}. This is important if the data is collected from the same subject during a long time period. Medical data is one example of such a data collection. Hash functions can, however, produce the same hash value for two different identifiers, so collisions might occur. Collisions can be mitigated by using collision resistant hash functions \cite{Abd:2014}. 

Hashes are vulnerable to rainbow attacks. In rainbow attack, a hacker is using pre-calculated hash values, rainbow tables. Rainbow tables contain an enormous amount of original identifiers with their hash values. If the attacker is able to find a matching hash value they get to know what is the corresponding original identifier. Salted hashing can be used to prevent rainbow attacks. When the salt value is unique for each subject it becomes computationally infeasible to calculate rainbow tables with each salt value. The principle of Salted Hashing is presented in Figure ~\ref{fig:k4}: The identifying attributes and a unique salt for a subject are given as input to a hash function that produces a fixed length hash value. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.40]{SaltedHashing.png}
\caption
{Salted Hashing}
\label{fig:k4}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
%\vspace*{1px}
The pseudonymization technique allows subject re-identification, so it permits more use cases for data than anonymization. 
The quasi-identifiers produce a linking-effect threat to unauthorized re-identification in a similar manner than in anonymization. The pseudonymization is thus often used with k-anonymity or l-diversity methods. If re-identifying the subject is necessary, then a mapping from original identifier to pseudonym must be stored. If the security of this mapping is compromised then the re-identification of subjects becomes trivial. Therefore, it is advisable to store the mapping only when absolutely necessary.

\newpage
%\vspace*{2px}
%-------------------------------------------------%
\section{Pseudonymization Proof of Concept}
%-------------------------------------------------%	 

Intel has published two separate reports of their experiences and learnings of pseudonymization. The first report describes how Intel implemented a proof of concept to protect the privacy of it's employees. In this experiment, they used data of a web service event logging system. This data was analyzed in a public cloud after it was pseudonymized \cite{Sed:2012}. In their report they use the term anonymization, but as the goal is to be able to do re-identification after the analysis, in our terminology it is pseudonymization. 
The second report describes experiences of pseudonymization in relation to Big Data \cite{Sed:2014}.

The experiment set-up is described in Figure~\ref{fig:k2}: Web service logs were collected in virtual machines. Pseudonymization was done on the same virtual machines that generated the studied logging data. This ensured that the data was already pseudonymized before it was sent to the public cloud.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.44]{IntelPoC.png}
\caption
{Intel Pseudonymization Proof of Concept }
\label{fig:k2}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newpage
%\vspace*{1px}
\newpage
The pseudonymization was done using open source tools by masking and encrypting IP addresses and by hashing URL's. Logs were analyzed in a public cloud the with a Log Management tool that was provided as a SaaS-level service by Cloud Provider. The re-identification, from pseudonyms to original identities, was done in a secure enclave,  but how the mapping table was transmitted there was not explained. According to their experiences, the data remained useful for both performance and security analysis after being pseudonymized \cite{Sed:2012}. 

A later study of Intel was done to test pseudonymization in relation to Big Data \cite{Sed:2014}. In this experiment, they created a k-anonymity based metrics to study the quality of the pseudonymization. The initial attempt to pseudonymize the data was vulnerable to correlation attacks. They were surprised to find out how web server log file had identifiers in several fields of the record \cite{Sed:2014}. 
Another surprising finding was the level of detailed information the user agent of a browser has \cite{Sed:2014}. They decided to modify the log files to improve the quality of pseudonymization. Log files were modified by removing references to the site, nodes, and language. Also, all user agent and browser information was removed. All time stamps were aggregated to intervals of 1 minute, 10 minutes or 1 hour. After all these generalizations they still had unique entries that involve a high risk of re-identification. 
Removing all entries with two or less identical entries, the risk of re-identification was significantly lowered \cite{Sed:2014}. They conclude by stating that "It would be useful to have tools that anonymize appropriately given a set of desired privacy and utility metrics, automating the analysis needed to figure out the best techniques for anonymization \cite{Sed:2014}."

These two experiments highlight that pseudonymization may seem easy at first, but it might require several iterations before getting it right. It is very usable observation is that k-anonymity can be used not only as a method to do pseudonymization or anonymization but also as an indicator how well the pseudonymization has worked. This idea can be adopted to anonymization and to the l-diversity method.  

\newpage
\section{Conclusions}

Enterprises are increasingly using public clouds for collecting, storing and computing data. In this work, we studied how anonymization and pseudonymization can be used to protect privacy in a public cloud environment.

Anonymization technique permanently removes such data attributes that identify the data subject and aims to modify quasi-identifiers in a way that re-identification is not possible. Anonymization is suitable for such data usage that does not require knowing the real identity of the subject. 

Pseudonymization technique replaces identifiers with pseudonyms. The quasi-identifiers needs to be modified similarily than in anonymization. Pseudonymization allows re-identification, making it more broadly applicable. Pseudonymization is also usable for long-term data collection where data items of the same subject need to be connected. 

Thorough understanding of managed data is necessary to perform anonymization or pseudonymization successfully. To retain data usable for needed operations it may be necessary to delimit the level of obscuring, generalizing or suppressing certain attributes. 

To avoid re-identification through linking effect it is necessary to gather an understanding of other related datasets. Anonymization and pseudonymization should not be stable activities but evolve with prevalent conditions. 

K-anonymity and l-diversity models have been designed to assist in anonymization, but they can be used also for pseudonymization and for measuring the achieved level of anonymization or pseudonymization. 

The privacy preservation should be taken seriously no matter if the data is used for enterprise's own business, opening it for research purposes of releasing it as open data. The data protection legislation gives boundaries how privacy should be managed. Once the dataset is successfully anonymized the data protection regulations no longer hold. 


The cloud providers are implementing security as part of their services. But as enterprises as cloud users are handling highly sensitive data, no method is safe enough to be used alone. Anonymization and pseudonymization are often used together with secure transmission channels, encryption, and authentication. Anonymization and pseudonymization are there to work as a backup if the other precautions fail. 

